import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PetService } from '../_services/pet.service';

@Injectable({
  providedIn: 'root'
})
export class PetResolverService implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.petService.getByAnimalName(route.paramMap.get('animalName'));
  }

  constructor(
    private petService: PetService
  ) { }
}
