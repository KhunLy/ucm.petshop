import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PetService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getByAnimalName(name:string): Observable<any> {
    return this.httpClient.get(environment.apiEndPoint + '/pet/byAnimal/' + name);
  }
}
