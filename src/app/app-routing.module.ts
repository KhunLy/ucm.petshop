import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './_components/home/home.component';
import { SecurityComponent } from './_components/security/security.component';
import { AnimalComponent } from './_components/animal/animal.component';
import { IsAdminGuard } from './_guards/is-admin.guard';
import { PetComponent } from './_components/pet/pet.component';
import { PetResolverService } from './_resolvers/pet-resolver.service';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'security', component: SecurityComponent },
  { path: 'animal', component: AnimalComponent, canActivate: [IsAdminGuard ]},
  { path: 'pet/:animalName', component: PetComponent, resolve: {petList: PetResolverService}},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
