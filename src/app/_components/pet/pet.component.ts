import { Component, OnInit, OnChanges, AfterContentInit, DoCheck } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PetService } from 'src/app/_services/pet.service';

@Component({
  selector: 'app-pet',
  templateUrl: './pet.component.html',
  styleUrls: ['./pet.component.scss']
})
export class PetComponent implements OnInit {

  list: any[];

  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(
      d => this.list = d['petList']
    )
  }

}
